<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 2018 &copy; Filmy Caravan Present's By
        <a target="_blank" href="#">KSS Box Office</a>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
<!-- END FOOTER -->